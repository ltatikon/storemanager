# Getting Started

### Prerequisites to run the application on your machine
* Java 8 and Maven
###  Run application 
* ./mvnw spring-boot:run
     <br /> (or) 
* Run StoremanagerApplication class as java application from IDE
     <br /> (or) 
* docker build using Dockerfile 
   NOTE : Make sure mvn package is executed before docker build  
   docker build --quiet --build-arg ENVIRONMENT=local  --tag storemanager:1.0.0 .
   docker run -d -p 8080:8080 storemanager:1.0.0
 
### Using the application
* Open http://localhost:8080/ to see home page with search options Latitude & Longitude  <br />
* The Latitude & Longitude input values are populated by current geo location when you allow the browser to access your computer geo location  <br />
* Click on Submit button to see five near by Jumbo stores based on Latitude and Longitude  <br />
 
 Application has all the crud API's to manage Stores  

### API Swagger documentation 
 * Link to API documentation (http://localhost:8080/swagger-ui/index.html#/)

### DATABASE
* Application uses in-memory h2 store (http://localhost:8080//h2-console)

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/reference/htmlsingle/#boot-features-spring-mvc-template-engines)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-nl.jumbo.storemanager.service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)

