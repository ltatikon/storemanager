package nl.jumbo.storemanager.repository;

import nl.jumbo.storemanager.StoreManagerApplication;
import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.enums.LocationType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        classes = {
                StoreManagerApplication.class
        },
        properties = {
                "spring.main.allow-bean-definition-overriding=true"
        })
public class StoreRepositoryTest {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private StoreRepository storeRepository;

    @Test
    @DisplayName("Should return five close by stores based on longitude and latitude")
    void findNearByStores() {
        //given
        Store store = new Store("uuid", "city", "1234AH", "street", "street2", "street3", "EOgKYx4XFiQAAAFJa_YYZ4At", 5.0, 52.0, "33249", true, "08:00", LocationType.PuP, true, "3605", "20:00", 0.0);
        storeRepository.save(store);
        //when
        List<Store> stores = storeRepository.findNearByStores(5.0, 52.0);

        //then

        assertEquals(0.0, stores.get(0).getDistance());
        assertEquals(5, stores.size());
        assertTrue(stores.get(0).getDistance() <= stores.get(1).getDistance());
        assertTrue(stores.get(1).getDistance() <= stores.get(2).getDistance());
        assertTrue(stores.get(2).getDistance() <= stores.get(3).getDistance());
        assertTrue(stores.get(3).getDistance() <= stores.get(4).getDistance());
    }
}
