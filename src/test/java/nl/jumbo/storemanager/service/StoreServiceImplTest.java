package nl.jumbo.storemanager.service;

import info.solidsoft.mockito.java8.api.WithBDDMockito;
import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.enums.LocationType;
import nl.jumbo.storemanager.domain.exception.StoreNotFoundException;
import nl.jumbo.storemanager.repository.StoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class StoreServiceImplTest implements WithBDDMockito {

    @Mock
    private StoreRepository storeRepository;
    private StoreService underTest;

    @BeforeEach
    void setUp() {
        underTest = new StoreServiceImpl(storeRepository);
    }

    @Test
    @DisplayName("Should return list of stores")
    void getStoreData() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        List<Store> stores = Collections.singletonList(store);
        given(storeRepository.findNearByStores(5.0, 52.0)).willReturn(Collections.singletonList(store));

        // when
        List<Store> result = underTest.getNearByStores(5.0, 52.0);

        // then
        assertEquals(result, stores);
        verify(storeRepository).findNearByStores(5.0, 52.0);
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should pass the unhandled exception")
    void getStoreData_failure() {
        // given
        given(storeRepository.findNearByStores(5.0, 52.0)).willThrow(RuntimeException.class);

        // when
        assertThrows(RuntimeException.class, () -> underTest.getNearByStores(5.0, 52.0));

        // then
        verify(storeRepository).findNearByStores(5.0, 52.0);
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should return list of stores")
    void getAllStores_success() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        List<Store> stores = Collections.singletonList(store);
        given(storeRepository.findAll()).willReturn(Collections.singletonList(store));

        // when
        List<Store> result = underTest.getStores();

        // then
        assertEquals(result, stores);
        verify(storeRepository).findAll();
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should return store based on uuid WHEN uuid is exist in repository")
    void getStore_success() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        given(storeRepository.findById("uuid")).willReturn(Optional.of(store));

        // when
        Store result = underTest.getStore("uuid");

        // then
        assertEquals(result, store);
        verify(storeRepository).findById("uuid");
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should throw Store not found exception WHEN uuid is not exist in repository")
    void getStore_when_not_found() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        given(storeRepository.findById("uuid")).willReturn(Optional.empty());

        // when
        assertThrows(StoreNotFoundException.class, () -> underTest.getStore("uuid"));

        // then
        verify(storeRepository).findById("uuid");
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should add Store to repository")
    void addStore() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);

        // when
        underTest.addStore(store);
        // then
        verify(storeRepository).save(store);
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    @DisplayName("Should delete Store from repository")
    void deleteStore() {
        // given

        // when
        underTest.deleteStore("uuid");
        // then
        verify(storeRepository).deleteById("uuid");
        verifyNoMoreInteractions(storeRepository);
    }


}
