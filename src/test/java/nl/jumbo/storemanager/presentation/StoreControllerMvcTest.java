package nl.jumbo.storemanager.presentation;

import info.solidsoft.mockito.java8.api.WithBDDMockito;
import nl.jumbo.storemanager.StoreManagerApplication;
import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.enums.LocationType;
import nl.jumbo.storemanager.service.StoreService;
import nl.jumbo.storemanager.util.JsonConverterUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StoreController.class)
@ContextConfiguration(classes = {StoreManagerApplication.class})
public class StoreControllerMvcTest implements WithBDDMockito {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private StoreService storeService;
    @Captor
    private ArgumentCaptor<Store> storeArgumentCaptor;

    @Test
    @DisplayName("SHOULD return list of stores json When request has longitude and latitude query params")
    void getStores_200() throws Exception {
        // given

        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        given(storeService.getNearByStores(52.3, 5.2)).willReturn(Collections.singletonList(store));
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("longitude", "52.3")
                        .param("latitude", "5.2"))
                .andReturn()
                .getResponse();
        // then
        assertEquals(200, response.getStatus());
        assertEquals("{\"_embedded\":{\"storeList\":[{\"uuid\":\"uuid\",\"city\":\"city\",\"postalCode\":\"postalCode\",\"street\":\"street\",\"street2\":\"street2\",\"street3\":\"street3\",\"addressName\":\"addressName\",\"longitude\":5.0,\"latitude\":52.0,\"complexNumber\":\"complexNumber\",\"showWarningMessage\":true,\"todayOpen\":\"todayOpen\",\"locationType\":\"Supermarkt\",\"collectionPoint\":true,\"sapStoreID\":\"sapStoreID\",\"todayClose\":\"todayClose\",\"distance\":0.0,\"_links\":{\"stores\":{\"href\":\"http://localhost/stores/uuid\"}}}]},\"_links\":{\"self\":{\"href\":\"http://localhost/stores?longitude=52.3&latitude=5.2\"}}}", response.getContentAsString());
        verify(storeService).getNearByStores(52.3, 5.2);
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return list of stores json When request has longitude and latitude query params")
    void getAllStores_200() throws Exception {
        // given

        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        given(storeService.getStores()).willReturn(Collections.singletonList(store));
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse();
        // then
        assertEquals(200, response.getStatus());
        assertEquals("{\"_embedded\":{\"storeList\":[{\"uuid\":\"uuid\",\"city\":\"city\",\"postalCode\":\"postalCode\",\"street\":\"street\",\"street2\":\"street2\",\"street3\":\"street3\",\"addressName\":\"addressName\",\"longitude\":5.0,\"latitude\":52.0,\"complexNumber\":\"complexNumber\",\"showWarningMessage\":true,\"todayOpen\":\"todayOpen\",\"locationType\":\"Supermarkt\",\"collectionPoint\":true,\"sapStoreID\":\"sapStoreID\",\"todayClose\":\"todayClose\",\"distance\":0.0,\"_links\":{\"stores\":{\"href\":\"http://localhost/stores/uuid\"}}}]},\"_links\":{\"self\":{\"href\":\"http://localhost/stores{?longitude,latitude}\",\"templated\":true}}}", response.getContentAsString());
        verify(storeService).getStores();
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request When request does not have latitude")
    void getStores_when_no_latitude_400() throws Exception {
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("longitude", "52.3"))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request When request does not have longitude")
    void getStores_when_no_longitude_400() throws Exception {
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("longitude", "5.2"))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request When longitude is invalid")
    void getStores_when_invalid_longitude_400() throws Exception {
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("longitude", "Not a valid number")
                        .param("latitude", "5.2"))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request When longitude is not in valid range")
    void getStores_when_invalid_range_longitude_400() throws Exception {
        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("longitude", "-181")
                        .param("latitude", "5.2"))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        assertTrue(response.getContentAsString().contains(" must be greater than or equal to -180"));
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return store based on uuid")
    void getStore_200() throws Exception {
        // given
        Store store = new Store("1234-1234-1234-1234-1234", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        given(storeService.getStore("1234-1234-1234-1234-1234")).willReturn(store);

        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores/{uuid}", "1234-1234-1234-1234-1234")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse();
        // then
        assertEquals(200, response.getStatus());
        assertEquals("{\"uuid\":\"1234-1234-1234-1234-1234\",\"city\":\"city\",\"postalCode\":\"postalCode\",\"street\":\"street\",\"street2\":\"street2\",\"street3\":\"street3\",\"addressName\":\"addressName\",\"longitude\":5.0,\"latitude\":52.0,\"complexNumber\":\"complexNumber\",\"showWarningMessage\":true,\"todayOpen\":\"todayOpen\",\"locationType\":\"Supermarkt\",\"collectionPoint\":true,\"sapStoreID\":\"sapStoreID\",\"todayClose\":\"todayClose\",\"distance\":0.0}", response.getContentAsString());
        verify(storeService).getStore("1234-1234-1234-1234-1234");
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request when UUID is not 24 characters")
    void getStore_400() throws Exception {
        // given

        // when
        MockHttpServletResponse response = mockMvc
                .perform(get("/stores/{uuid}", "uuid")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD add store to repository when all validation are passed")
    void addStore_200() throws Exception {
        // given
        Store store = new Store("1234-1234-1234-1234-1234", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);

        // when
        MockHttpServletResponse response = mockMvc
                .perform(post("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(JsonConverterUtil.convertToJson(store)))
                .andReturn()
                .getResponse();
        // then
        assertEquals(201, response.getStatus());
        verify(storeService).addStore(storeArgumentCaptor.capture());
        assertEquals(JsonConverterUtil.convertToJson(store), JsonConverterUtil.convertToJson(storeArgumentCaptor.getValue()));
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad request when latitude is not in range")
    void addStore_400() throws Exception {
        // given
        Store store = new Store("1234-1234-1234-1234-1234", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 91.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);

        // when
        MockHttpServletResponse response = mockMvc
                .perform(post("/stores")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(JsonConverterUtil.convertToJson(store)))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);

    }

    @Test
    @DisplayName("SHOULD return 204 when delete is successfull")
    void deleteStore_200() throws Exception {
        // given

        // when
        MockHttpServletResponse response = mockMvc
                .perform(delete("/stores/{uuid}", "1234-1234-1234-1234-1234")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse();
        // then
        assertEquals(200, response.getStatus());
        verify(storeService).deleteStore("1234-1234-1234-1234-1234");
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @DisplayName("SHOULD return bad delete request when UUID is not 24 characters")
    void deleteStore_400() throws Exception {
        // given

        // when
        MockHttpServletResponse response = mockMvc
                .perform(delete("/stores/{uuid}", "uuid")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse();
        // then
        assertEquals(400, response.getStatus());
        verifyNoMoreInteractions(storeService);
    }
}
