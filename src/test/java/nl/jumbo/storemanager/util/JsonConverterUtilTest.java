package nl.jumbo.storemanager.util;

import info.solidsoft.mockito.java8.api.WithBDDMockito;
import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.enums.LocationType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JsonConverterUtilTest implements WithBDDMockito {

    @Test
    @DisplayName("Should convert Name to Json")
    void convertToJson() {
        // given
        Store store = new Store("uuid", "city", "postalCode", "street", "street2", "street3", "addressName", 5.0, 52.0, "complexNumber", true, "todayOpen", LocationType.Supermarkt, true, "sapStoreID", "todayClose", 0.0);
        // when
        String json = JsonConverterUtil.convertToJson(store);

        // then
        assertEquals("{\"uuid\":\"uuid\",\"city\":\"city\",\"postalCode\":\"postalCode\",\"street\":\"street\",\"street2\":\"street2\",\"street3\":\"street3\",\"addressName\":\"addressName\",\"longitude\":5.0,\"latitude\":52.0,\"complexNumber\":\"complexNumber\",\"showWarningMessage\":true,\"todayOpen\":\"todayOpen\",\"locationType\":\"Supermarkt\",\"collectionPoint\":true,\"sapStoreID\":\"sapStoreID\",\"todayClose\":\"todayClose\",\"distance\":0.0,\"links\":[]}", json);
    }

    @Test
    @DisplayName("Should throw JsonConversionException while converting mock object")
    void exceptionConvToJson() {
        Store notSerilizableObj = mock(Store.class);

        assertThrows(JsonConversionException.class, () -> JsonConverterUtil.convertToJson(notSerilizableObj));
    }

    @Test
    @DisplayName("Should instantiate json to given object")
    void convertFromJson() {
        // given
        String json = "{\"uuid\":\"uuid\",\"city\":\"city\",\"street\":\"street\",\"street2\":\"street2\",\"street3\":\"street3\",\"addressName\":\"addressName\",\"longitude\":5.0,\"latitude\":52.0,\"complexNumber\":\"complexNumber\",\"showWarningMessage\":true,\"todayOpen\":\"todayOpen\",\"locationType\":\"Supermarkt\",\"collectionPoint\":true,\"sapStoreID\":\"sapStoreID\",\"todayClose\":\"todayClose\",\"distance\":0.0}";

        // when
        Store store = JsonConverterUtil.convertFromJson(json, Store.class);

        // then
        assertEquals("city", store.getCity());
        assertEquals("uuid", store.getUuid());
        assertNull(store.getPostalCode());
    }

    @Test
    @DisplayName("Should throw JsonConversionException on converting invalid json")
    void exceptionTestFromJson() {
        String input = "invalid json";

        assertThrows(JsonConversionException.class, () -> JsonConverterUtil.convertFromJson(input, Store.class));
    }

}
