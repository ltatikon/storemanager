package nl.jumbo.storemanager;

import nl.jumbo.storemanager.repository.StoreRepository;
import nl.jumbo.storemanager.service.StoreService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(
        classes = {
                StoreManagerApplication.class
        },
        properties = {
                "spring.main.allow-bean-definition-overriding=true"
        })
@ExtendWith(SpringExtension.class)
class StoremanagerApplicationTests {
    @Autowired
    ApplicationContext applicationContext;

    @Test
    void contextLoads() {
        assertNotNull(applicationContext.getBean("storeServiceImpl", StoreService.class));
        assertNotNull(applicationContext.getBean("storeRepository", StoreRepository.class));
    }

    @Test
    @DisplayName("SHOULD run without exceptions WHEN application is started")
    void main() {
        StoreManagerApplication.main(new String[]{});
    }

}
