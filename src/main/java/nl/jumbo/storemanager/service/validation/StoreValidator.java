package nl.jumbo.storemanager.service.validation;

import nl.jumbo.storemanager.domain.Store;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is invoked when {@link Store} is annotated with {@link StoreConstraint} annotation
 * This class Validates instance of {@link Store}
 * Service adds the validation failed message to {@link ConstraintValidatorContext} when violations exist
 */
public class StoreValidator implements ConstraintValidator<StoreConstraint, Store> {

    @Override
    public boolean isValid(Store store, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = true;


        if (store != null && (store.getLatitude() > 90 || store.getLatitude() < -90)) {
            addViolation(constraintValidatorContext, "must be between -90 and 90", "latitude");
            isValid = false;
        }
        if (store != null && (store.getLongitude() > 180 || store.getLongitude() < -180)) {
            addViolation(constraintValidatorContext, "must be between -180 and 180", "longitude");
            isValid = false;
        }


        return isValid;
    }

    private void addViolation(ConstraintValidatorContext constraintValidatorContext, String message, String fieldName) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(message)
                .addPropertyNode(fieldName)
                .addConstraintViolation();
    }

}
