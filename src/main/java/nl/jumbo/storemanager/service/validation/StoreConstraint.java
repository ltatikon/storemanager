package nl.jumbo.storemanager.service.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Whenever any instance of {@link nl.jumbo.storemanager.domain.Store} is annotated with this will be validated by {@link StoreValidator}
 */
@Documented
@Constraint(validatedBy = StoreValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface StoreConstraint {
    String message() default "Store data validation(s) failed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
