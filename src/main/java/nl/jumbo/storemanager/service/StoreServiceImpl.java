package nl.jumbo.storemanager.service;

import lombok.extern.slf4j.Slf4j;
import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.exception.StoreNotFoundException;
import nl.jumbo.storemanager.repository.StoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class StoreServiceImpl implements StoreService {
    private final StoreRepository storeRepository;

    public StoreServiceImpl(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    @Override
    public List<Store> getNearByStores(double longitude, double latitude) {
        log.debug("Search based on longitude {} and latitude {} ", longitude, latitude);
        return storeRepository.findNearByStores(longitude, latitude);
    }

    @Override
    public void deleteStore(String uuid) {
        storeRepository.deleteById(uuid);
    }

    @Override
    public List<Store> getStores() {
        return storeRepository.findAll();
    }

    @Override
    public Store getStore(String uuid) {
        return storeRepository.findById(uuid).orElseThrow(() -> new StoreNotFoundException("Store with uuid " + uuid + " not found"));
    }

    @Override
    public void addStore(Store store) {
        storeRepository.save(store);
    }
}
