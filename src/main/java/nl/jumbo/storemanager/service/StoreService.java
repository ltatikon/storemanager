package nl.jumbo.storemanager.service;

import nl.jumbo.storemanager.domain.Store;

import java.util.List;

public interface StoreService {
    /**
     * Service to retrieve stores from database based on longitude and latitude
     *
     * @param longitude longitude user input value
     * @param latitude  latitude user input value
     * @return list of stores near to longitude and latitude
     */
    List<Store> getNearByStores(double longitude, double latitude);

    /**
     * Service to retrieve all stores from database
     *
     * @return list of stores near to longitude and latitude
     */
    List<Store> getStores();

    /**
     * Service returns store data based on uuid
     *
     * @param uuid unique id of the store
     * @return store data
     */
    Store getStore(String uuid);


    /**
     * service is used to delete store from database
     *
     * @param uuid unique id of the store
     */
    void deleteStore(String uuid);

    /**
     * service add new store to database
     *
     * @param store new store object
     * @return
     */
    void addStore(Store store);
}
