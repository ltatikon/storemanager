package nl.jumbo.storemanager.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import nl.jumbo.storemanager.domain.enums.LocationType;
import nl.jumbo.storemanager.service.validation.StoreConstraint;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@StoreConstraint
public class Store extends RepresentationModel<Store> {
    @Id
    private String uuid;
    private String city;
    private String postalCode;
    private String street;
    private String street2;
    private String street3;
    private String addressName;
    @NotNull
    private Double longitude;
    @NotNull
    private Double latitude;
    private String complexNumber;
    private Boolean showWarningMessage;
    private String todayOpen;
    @Enumerated(EnumType.STRING)
    private LocationType locationType;
    private Boolean collectionPoint;
    private String sapStoreID;
    private String todayClose;
    private Double distance;
}
