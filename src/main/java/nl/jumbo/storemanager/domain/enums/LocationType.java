package nl.jumbo.storemanager.domain.enums;

import lombok.ToString;

@ToString
public enum LocationType {
    Supermarkt, SupermarktPuP, PuP
}
