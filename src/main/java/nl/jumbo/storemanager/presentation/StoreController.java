package nl.jumbo.storemanager.presentation;

import nl.jumbo.storemanager.domain.Store;
import nl.jumbo.storemanager.domain.exception.MissingParameterException;
import nl.jumbo.storemanager.service.StoreService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Validated
public class StoreController {
    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping(path = "/stores", produces = MediaType.APPLICATION_JSON_VALUE)
    public CollectionModel<Store> getStores(@RequestParam(name = "longitude", required = false) @Valid @Min(-180) @Max(180) Double longitude,
                                            @RequestParam(name = "latitude", required = false) @Valid @Min(-90) @Max(90) Double latitude) {
        List<Store> stores;
        if (longitude != null && latitude != null) {
            stores = storeService.getNearByStores(longitude, latitude);
        } else {
            if (longitude != null || latitude != null) {
                throw new MissingParameterException("Latitude and Longitude both parameters should come together ");
            }
            stores = storeService.getStores();
        }
        stores.forEach(store -> {
            store.add(linkTo(methodOn(StoreController.class)
                    .getStore(store.getUuid())).withRel("stores"));
        });
        return CollectionModel.of(stores, linkTo(methodOn(StoreController.class).getStores(longitude, latitude)).withSelfRel());
    }

    @GetMapping(path = "/stores/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Store getStore(@PathVariable @Size(min = 24, max = 24) String uuid) {
        return storeService.getStore(uuid);
    }

    @PostMapping(path = "/stores", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addStore(@RequestBody @Valid Store store) {
        storeService.addStore(store);
    }

    @DeleteMapping(path = "/stores/{uuid}")
    public void deleteStore(@PathVariable @Size(min = 24, max = 24) String uuid) {
        storeService.deleteStore(uuid);
    }

}
