package nl.jumbo.storemanager.repository;

import nl.jumbo.storemanager.domain.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StoreRepository extends JpaRepository<Store, String> {

    /*  69.1 is the conversion factor for miles to latitude degrees. 57.3 is roughly 180/pi,
    so that's conversion from degrees to radians, for the cosine function. 25 is the search radius in miles.
    This is the formula to use when using decimal degrees and statute miles*/

    /**
     * @param longitudeInput user longitude input
     * @param latitudeInput  user latitude input
     * @return list of near by 5 stores to the mentioned longitude and latitude input
     */
    @Query(value = "SELECT s.uuid,s.city,s.postalCode,s.street,s.street2,s.street3,s.addressName,s.longitude,s.latitude,s.complexNumber,s.showWarningMessage,s.todayOpen,s.locationType,s.collectionPoint,s.sapStoreID,s.todayClose, SQRT(POWER(69.1 * (s.latitude - ?2), 2) + POWER(69.1 * (?1 -s.longitude) * COS(s.latitude / 57.3), 2)) AS distance FROM Store s ORDER BY distance ASC LIMIT 5", nativeQuery = true)
    List<Store> findNearByStores(@Param("longitudeInput") double longitudeInput, @Param("latitudeInput") double latitudeInput);

}
