var HttpClient = function () {
    this.get = function (aUrl, aCallback) {
        let anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        // x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    document.getElementById("latitude").value = position.coords.latitude;
    document.getElementById("longitude").value = position.coords.longitude;
}


function getStores() {
    let latitude = document.getElementById("latitude").value;
    if (isEmpty(latitude)) {
        alert("Please enter latitude")
        return false;
    }
    if (isNaN(latitude) || latitude < -90 || latitude > 90) {
        alert("Please enter latitude value between -90 and 90")
        return false;
    }

    let longitude = document.getElementById("longitude").value;
    if (isEmpty(longitude)) {
        alert("Please enter longitude")
        return false;
    }
    if (isNaN(longitude) || longitude < -180 || longitude > 180) {
        alert("Please enter latitude value between -90 and 90")
        return false;
    }

    const client = new HttpClient();
    let getStoresURL = "http://localhost:8080/stores?latitude=" + latitude + "&longitude=" + longitude;

    client.get(getStoresURL, function (response) {
        let responseJSON = JSON.parse(response);
        let stores = responseJSON._embedded.storeList;
        let tableRef = document.getElementById('storeTable').getElementsByTagName('tbody')[0];
        tableRef.innerHTML = '';
        for (let i = 0; i < stores.length; i++) {
            let newRow = tableRef.insertRow();
            let currentStore = stores[i];
            addCell(newRow, 0, currentStore.uuid);
            addCell(newRow, 1, currentStore.city);
            addCell(newRow, 2, currentStore.postalCode);
            addCell(newRow, 3, currentStore.street);
            addCell(newRow, 4, currentStore.street2);
            addCell(newRow, 5, currentStore.street3);
            addCell(newRow, 6, currentStore.addressName);
            addCell(newRow, 7, currentStore.longitude);
            addCell(newRow, 8, currentStore.latitude);
            addCell(newRow, 9, currentStore.complexNumber);
            addCell(newRow, 10, currentStore.showWarningMessage);
            addCell(newRow, 11, currentStore.todayOpen);
            addCell(newRow, 12, currentStore.locationType);
            addCell(newRow, 13, currentStore.sapStoreID);
            addCell(newRow, 14, currentStore.todayClose);
            addCell(newRow, 15, currentStore.distance);
        }
    });
}

function addCell(row, index, value) {
    var cell = row.insertCell(index);
    if (value) {
        cell.appendChild(document.createTextNode(value));
    } else {
        cell.appendChild(document.createTextNode(""));
    }
}

function isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}
